import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import {Link} from 'react-router-dom';
export function NavigationBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Demo App
          </Typography>
          <Button color="inherit"><Link to='/' style={{textDecoration:'none',color:'white'}}>Home</Link></Button>
          <Button color="inherit"><Link to='/about' style={{textDecoration:'none',color:'white'}}>About</Link></Button>
          <Button color="inherit"><Link to='/contact' style={{textDecoration:'none',color:'white'}}>Contact</Link></Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}